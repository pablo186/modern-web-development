# Modern Web Development

Bootstrap a fullstack Modern Web App with:

- Docker (compose).
- TypeScript, Prettier, and Husky.
- Express, Prisma (GraphQL: Data Modeling), Nexus, and Pal.
- React (CRA).

![Modern Web Development](/help/apollo-studio-mutation.png)

### Backend help

- https://expressjs.com
- https://www.prisma.io/docs
- https://www.prisma.io/apollo
- https://nexusjs.org/docs
- https://paljs.com/

![Internals](/help/internals.png)

GraphQL Security Checklist:

- https://www.apollographql.com/blog/graphql/security/9-ways-to-secure-your-graphql-api-security-checklist
- https://www.apollographql.com/blog/graphql/security/securing-your-graphql-api-from-malicious-queries

- https://www.apollographql.com/blog/graphql/security/why-you-should-disable-graphql-introspection-in-production

### Frontend help

CRA:

- https://create-react-app.dev/docs/getting-started/#creating-a-typescript-app

React TypeScript Cheatsheet:

- https://www.josoroma.com/ts-cheatsheet

- https://react-typescript-cheatsheet.netlify.app/docs/basic/getting-started/basic_type_example

# Setting up your Dev Env

Use environment variables (required)

```
code .env
```

```
DATABASE_URL=postgresql://postgres:password@localhost:54325/postgres
```

![.env](/help/env.png)

First and foremost, please set up your **JS Development Environment**:

- https://www.josoroma.com/setting-up-your-js-dev-env

### Install App packages

- https://classic.yarnpkg.com/en/docs/usage

```
yarn
```

### Build db and server services

- https://docs.docker.com/compose/reference/build

```
docker compose build db server
```

### Starts db and server services

- https://docs.docker.com/compose/reference/up

```
docker compose up db server
```

### Prisma migrate (Migrations)

- https://www.prisma.io/docs/concepts/components/prisma-migrate

```
yarn run migrate
```

![yarn run migrate](/help/yarn-run-migrate.png)

### Prisma seed (Seeding)

- https://www.prisma.io/docs/guides/database/seed-database

```
yarn run seed
```

### Backend tests

```
yarn test:server
```

It is recommended to use this approach (mocking the Prisma Client):

- https://www.prisma.io/docs/guides/testing/unit-testing

![test server](/help/yarn-test-server.png)

## Start your React TypeScript App locally

- http://localhost:3000

React Router Components (Pages):

- http://localhost:3000/dashboard/user (GQL Request)
- http://localhost:3000/dashboard
- http://localhost:3000/home

```
yarn run client:dev
```

![CRA](/help/client.png)

`Pages` and private or public `Layouts` approach is so flexible that they can be subdivided into `Containers` and `Presentational` components if you want.

TODO: Serve the React App with nginx in Docker.

## Launch Your Prisma Studio

```
yarn studio
```

![Prisma Studio](/help/yarn-studio.png)

Note: Prisma Studio is up on: http://localhost:5555

## Please visit Your Apollo GraphQL Studio

- http://localhost:3001/graphql

![Apollo GraphQL Studio](/help/apollo-studio-query.png)

## It would be nice to have

- Authentication with JWT (token + refresh).

- Authorization with Oso.

![oso](/help/oso.png)

# Happy hacking!!!
