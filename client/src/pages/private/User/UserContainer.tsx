import { memo, useMemo } from 'react'
import { User } from './User'
import { useUserFindManyQuery } from 'graphql/generated'

export const UserContainer = () => {
  const { data: userData, error: userError, loading: isUserLoading } = useUserFindManyQuery()

  const data = useMemo(
    () => (userData?.userFindManyQuery ? userData.userFindManyQuery : []),
    [userData?.userFindManyQuery],
  )

  if (isUserLoading) return <>Loading Component</>
  if (userError) return <>Error Component</>
  if (!userData?.userFindManyQuery) return <>No Data Component</>

  return (
    <>
      <User data={data} />
    </>
  )
}

export const MemoizedUserContainer = memo(UserContainer)
