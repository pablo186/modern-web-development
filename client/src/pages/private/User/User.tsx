export type TData = {
  __typename?: 'User'
  id: string
  email: string
  firstName: string
  lastName: string
}

export interface IUser {
  data: TData[]
}

export const User = (props: IUser) => {
  const { data } = props

  return (
    <>
      {data && (
        <ul>
          {data.map((data: TData) => (
            <li key={data.id}>{data.email}</li>
          ))}
        </ul>
      )}
    </>
  )
}
