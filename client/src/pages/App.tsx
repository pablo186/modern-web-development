import { ApolloProvider } from '@apollo/client'

import { PagesRouter } from 'pages/PagesRouter'
import { apolloClient } from 'clients'

export const App = () => (
  <>
    <ApolloProvider client={apolloClient}>
      <PagesRouter />
    </ApolloProvider>
  </>
)
