import { Routes, Route, BrowserRouter } from 'react-router-dom'

import { PrivateLayout, PublicLayout } from 'layouts'

import { Dashboard, User } from './private'
import { Home } from './public'

export const PagesRouter = () => (
  <BrowserRouter>
    <Routes>
      {/* Public Routes */}
      <Route
        path="/"
        element={
          <PublicLayout>
            <Home />
          </PublicLayout>
        }
      >
        <Route
          index
          element={
            <PublicLayout>
              <Home />
            </PublicLayout>
          }
        />
      </Route>
      {/* Private Routes */}
      <Route
        path="/dashboard"
        element={
          <PrivateLayout>
            <Dashboard />
          </PrivateLayout>
        }
      >
        <Route
          index
          element={
            <PrivateLayout>
              <Dashboard />
            </PrivateLayout>
          }
        />
      </Route>
      <Route
        path="/dashboard/user"
        element={
          <PrivateLayout>
            <User />
          </PrivateLayout>
        }
      />
    </Routes>
  </BrowserRouter>
)
