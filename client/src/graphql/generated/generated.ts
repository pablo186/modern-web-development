import { gql } from '@apollo/client'
import * as Apollo from '@apollo/client'
export type Maybe<T> = T | null
export type InputMaybe<T> = Maybe<T>
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] }
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> }
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> }
const defaultOptions = {} as const
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string
  String: string
  Boolean: boolean
  Int: number
  Float: number
  /** BigInt custom scalar type */
  BigInt: any
  /** Date custom scalar type */
  DateTime: any
  /** Decimal custom scalar type */
  Decimal: any
  /** Json custom scalar type */
  Json: any
}

export type AggregateUser = {
  __typename?: 'AggregateUser'
  _count?: Maybe<UserCountAggregateOutputType>
  _max?: Maybe<UserMaxAggregateOutputType>
  _min?: Maybe<UserMinAggregateOutputType>
}

export type BatchPayload = {
  __typename?: 'BatchPayload'
  count: Scalars['Int']
}

export type DateTimeFieldUpdateOperationsInput = {
  set?: InputMaybe<Scalars['DateTime']>
}

export type DateTimeFilter = {
  equals?: InputMaybe<Scalars['DateTime']>
  gt?: InputMaybe<Scalars['DateTime']>
  gte?: InputMaybe<Scalars['DateTime']>
  in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>
  lt?: InputMaybe<Scalars['DateTime']>
  lte?: InputMaybe<Scalars['DateTime']>
  not?: InputMaybe<NestedDateTimeFilter>
  notIn?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>
}

export type DateTimeNullableFilter = {
  equals?: InputMaybe<Scalars['DateTime']>
  gt?: InputMaybe<Scalars['DateTime']>
  gte?: InputMaybe<Scalars['DateTime']>
  in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>
  lt?: InputMaybe<Scalars['DateTime']>
  lte?: InputMaybe<Scalars['DateTime']>
  not?: InputMaybe<NestedDateTimeNullableFilter>
  notIn?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>
}

export type DateTimeNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>
  _max?: InputMaybe<NestedDateTimeNullableFilter>
  _min?: InputMaybe<NestedDateTimeNullableFilter>
  equals?: InputMaybe<Scalars['DateTime']>
  gt?: InputMaybe<Scalars['DateTime']>
  gte?: InputMaybe<Scalars['DateTime']>
  in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>
  lt?: InputMaybe<Scalars['DateTime']>
  lte?: InputMaybe<Scalars['DateTime']>
  not?: InputMaybe<NestedDateTimeNullableWithAggregatesFilter>
  notIn?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>
}

export type DateTimeWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>
  _max?: InputMaybe<NestedDateTimeFilter>
  _min?: InputMaybe<NestedDateTimeFilter>
  equals?: InputMaybe<Scalars['DateTime']>
  gt?: InputMaybe<Scalars['DateTime']>
  gte?: InputMaybe<Scalars['DateTime']>
  in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>
  lt?: InputMaybe<Scalars['DateTime']>
  lte?: InputMaybe<Scalars['DateTime']>
  not?: InputMaybe<NestedDateTimeWithAggregatesFilter>
  notIn?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>
}

export type Mutation = {
  __typename?: 'Mutation'
  userCreateOneMutation: User
}

export type MutationUserCreateOneMutationArgs = {
  data: UserCreateInput
}

export type NestedDateTimeFilter = {
  equals?: InputMaybe<Scalars['DateTime']>
  gt?: InputMaybe<Scalars['DateTime']>
  gte?: InputMaybe<Scalars['DateTime']>
  in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>
  lt?: InputMaybe<Scalars['DateTime']>
  lte?: InputMaybe<Scalars['DateTime']>
  not?: InputMaybe<NestedDateTimeFilter>
  notIn?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>
}

export type NestedDateTimeNullableFilter = {
  equals?: InputMaybe<Scalars['DateTime']>
  gt?: InputMaybe<Scalars['DateTime']>
  gte?: InputMaybe<Scalars['DateTime']>
  in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>
  lt?: InputMaybe<Scalars['DateTime']>
  lte?: InputMaybe<Scalars['DateTime']>
  not?: InputMaybe<NestedDateTimeNullableFilter>
  notIn?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>
}

export type NestedDateTimeNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>
  _max?: InputMaybe<NestedDateTimeNullableFilter>
  _min?: InputMaybe<NestedDateTimeNullableFilter>
  equals?: InputMaybe<Scalars['DateTime']>
  gt?: InputMaybe<Scalars['DateTime']>
  gte?: InputMaybe<Scalars['DateTime']>
  in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>
  lt?: InputMaybe<Scalars['DateTime']>
  lte?: InputMaybe<Scalars['DateTime']>
  not?: InputMaybe<NestedDateTimeNullableWithAggregatesFilter>
  notIn?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>
}

export type NestedDateTimeWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>
  _max?: InputMaybe<NestedDateTimeFilter>
  _min?: InputMaybe<NestedDateTimeFilter>
  equals?: InputMaybe<Scalars['DateTime']>
  gt?: InputMaybe<Scalars['DateTime']>
  gte?: InputMaybe<Scalars['DateTime']>
  in?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>
  lt?: InputMaybe<Scalars['DateTime']>
  lte?: InputMaybe<Scalars['DateTime']>
  not?: InputMaybe<NestedDateTimeWithAggregatesFilter>
  notIn?: InputMaybe<Array<InputMaybe<Scalars['DateTime']>>>
}

export type NestedIntFilter = {
  equals?: InputMaybe<Scalars['Int']>
  gt?: InputMaybe<Scalars['Int']>
  gte?: InputMaybe<Scalars['Int']>
  in?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>
  lt?: InputMaybe<Scalars['Int']>
  lte?: InputMaybe<Scalars['Int']>
  not?: InputMaybe<NestedIntFilter>
  notIn?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>
}

export type NestedIntNullableFilter = {
  equals?: InputMaybe<Scalars['Int']>
  gt?: InputMaybe<Scalars['Int']>
  gte?: InputMaybe<Scalars['Int']>
  in?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>
  lt?: InputMaybe<Scalars['Int']>
  lte?: InputMaybe<Scalars['Int']>
  not?: InputMaybe<NestedIntNullableFilter>
  notIn?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>
}

export type NestedStringFilter = {
  contains?: InputMaybe<Scalars['String']>
  endsWith?: InputMaybe<Scalars['String']>
  equals?: InputMaybe<Scalars['String']>
  gt?: InputMaybe<Scalars['String']>
  gte?: InputMaybe<Scalars['String']>
  in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>
  lt?: InputMaybe<Scalars['String']>
  lte?: InputMaybe<Scalars['String']>
  not?: InputMaybe<NestedStringFilter>
  notIn?: InputMaybe<Array<InputMaybe<Scalars['String']>>>
  startsWith?: InputMaybe<Scalars['String']>
}

export type NestedStringWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>
  _max?: InputMaybe<NestedStringFilter>
  _min?: InputMaybe<NestedStringFilter>
  contains?: InputMaybe<Scalars['String']>
  endsWith?: InputMaybe<Scalars['String']>
  equals?: InputMaybe<Scalars['String']>
  gt?: InputMaybe<Scalars['String']>
  gte?: InputMaybe<Scalars['String']>
  in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>
  lt?: InputMaybe<Scalars['String']>
  lte?: InputMaybe<Scalars['String']>
  not?: InputMaybe<NestedStringWithAggregatesFilter>
  notIn?: InputMaybe<Array<InputMaybe<Scalars['String']>>>
  startsWith?: InputMaybe<Scalars['String']>
}

export type NullableDateTimeFieldUpdateOperationsInput = {
  set?: InputMaybe<Scalars['DateTime']>
}

export type Query = {
  __typename?: 'Query'
  userFindManyQuery: Array<User>
}

export type QueryUserFindManyQueryArgs = {
  where?: InputMaybe<UserWhereInput>
}

export enum QueryMode {
  Default = 'default',
  Insensitive = 'insensitive',
}

export enum SortOrder {
  Asc = 'asc',
  Desc = 'desc',
}

export type StringFieldUpdateOperationsInput = {
  set?: InputMaybe<Scalars['String']>
}

export type StringFilter = {
  contains?: InputMaybe<Scalars['String']>
  endsWith?: InputMaybe<Scalars['String']>
  equals?: InputMaybe<Scalars['String']>
  gt?: InputMaybe<Scalars['String']>
  gte?: InputMaybe<Scalars['String']>
  in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>
  lt?: InputMaybe<Scalars['String']>
  lte?: InputMaybe<Scalars['String']>
  mode?: InputMaybe<QueryMode>
  not?: InputMaybe<NestedStringFilter>
  notIn?: InputMaybe<Array<InputMaybe<Scalars['String']>>>
  startsWith?: InputMaybe<Scalars['String']>
}

export type StringWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>
  _max?: InputMaybe<NestedStringFilter>
  _min?: InputMaybe<NestedStringFilter>
  contains?: InputMaybe<Scalars['String']>
  endsWith?: InputMaybe<Scalars['String']>
  equals?: InputMaybe<Scalars['String']>
  gt?: InputMaybe<Scalars['String']>
  gte?: InputMaybe<Scalars['String']>
  in?: InputMaybe<Array<InputMaybe<Scalars['String']>>>
  lt?: InputMaybe<Scalars['String']>
  lte?: InputMaybe<Scalars['String']>
  mode?: InputMaybe<QueryMode>
  not?: InputMaybe<NestedStringWithAggregatesFilter>
  notIn?: InputMaybe<Array<InputMaybe<Scalars['String']>>>
  startsWith?: InputMaybe<Scalars['String']>
}

export type User = {
  __typename?: 'User'
  created: Scalars['DateTime']
  email: Scalars['String']
  firstName: Scalars['String']
  id: Scalars['String']
  lastName: Scalars['String']
  updated: Scalars['DateTime']
}

export type UserCountAggregateOutputType = {
  __typename?: 'UserCountAggregateOutputType'
  _all: Scalars['Int']
  created: Scalars['Int']
  email: Scalars['Int']
  firstName: Scalars['Int']
  id: Scalars['Int']
  lastLogin: Scalars['Int']
  lastName: Scalars['Int']
  password: Scalars['Int']
  updated: Scalars['Int']
}

export type UserCountOrderByAggregateInput = {
  created?: InputMaybe<SortOrder>
  email?: InputMaybe<SortOrder>
  firstName?: InputMaybe<SortOrder>
  id?: InputMaybe<SortOrder>
  lastLogin?: InputMaybe<SortOrder>
  lastName?: InputMaybe<SortOrder>
  password?: InputMaybe<SortOrder>
  updated?: InputMaybe<SortOrder>
}

export type UserCreateInput = {
  created?: InputMaybe<Scalars['DateTime']>
  email: Scalars['String']
  firstName?: InputMaybe<Scalars['String']>
  id?: InputMaybe<Scalars['String']>
  lastLogin?: InputMaybe<Scalars['DateTime']>
  lastName?: InputMaybe<Scalars['String']>
  password: Scalars['String']
  updated?: InputMaybe<Scalars['DateTime']>
}

export type UserCreateManyInput = {
  created?: InputMaybe<Scalars['DateTime']>
  email: Scalars['String']
  firstName?: InputMaybe<Scalars['String']>
  id?: InputMaybe<Scalars['String']>
  lastLogin?: InputMaybe<Scalars['DateTime']>
  lastName?: InputMaybe<Scalars['String']>
  password: Scalars['String']
  updated?: InputMaybe<Scalars['DateTime']>
}

export type UserMaxAggregateOutputType = {
  __typename?: 'UserMaxAggregateOutputType'
  created?: Maybe<Scalars['DateTime']>
  email?: Maybe<Scalars['String']>
  firstName?: Maybe<Scalars['String']>
  id?: Maybe<Scalars['String']>
  lastLogin?: Maybe<Scalars['DateTime']>
  lastName?: Maybe<Scalars['String']>
  password?: Maybe<Scalars['String']>
  updated?: Maybe<Scalars['DateTime']>
}

export type UserMaxOrderByAggregateInput = {
  created?: InputMaybe<SortOrder>
  email?: InputMaybe<SortOrder>
  firstName?: InputMaybe<SortOrder>
  id?: InputMaybe<SortOrder>
  lastLogin?: InputMaybe<SortOrder>
  lastName?: InputMaybe<SortOrder>
  password?: InputMaybe<SortOrder>
  updated?: InputMaybe<SortOrder>
}

export type UserMinAggregateOutputType = {
  __typename?: 'UserMinAggregateOutputType'
  created?: Maybe<Scalars['DateTime']>
  email?: Maybe<Scalars['String']>
  firstName?: Maybe<Scalars['String']>
  id?: Maybe<Scalars['String']>
  lastLogin?: Maybe<Scalars['DateTime']>
  lastName?: Maybe<Scalars['String']>
  password?: Maybe<Scalars['String']>
  updated?: Maybe<Scalars['DateTime']>
}

export type UserMinOrderByAggregateInput = {
  created?: InputMaybe<SortOrder>
  email?: InputMaybe<SortOrder>
  firstName?: InputMaybe<SortOrder>
  id?: InputMaybe<SortOrder>
  lastLogin?: InputMaybe<SortOrder>
  lastName?: InputMaybe<SortOrder>
  password?: InputMaybe<SortOrder>
  updated?: InputMaybe<SortOrder>
}

export type UserOrderByWithAggregationInput = {
  _count?: InputMaybe<UserCountOrderByAggregateInput>
  _max?: InputMaybe<UserMaxOrderByAggregateInput>
  _min?: InputMaybe<UserMinOrderByAggregateInput>
  created?: InputMaybe<SortOrder>
  email?: InputMaybe<SortOrder>
  firstName?: InputMaybe<SortOrder>
  id?: InputMaybe<SortOrder>
  lastLogin?: InputMaybe<SortOrder>
  lastName?: InputMaybe<SortOrder>
  password?: InputMaybe<SortOrder>
  updated?: InputMaybe<SortOrder>
}

export type UserOrderByWithRelationInput = {
  created?: InputMaybe<SortOrder>
  email?: InputMaybe<SortOrder>
  firstName?: InputMaybe<SortOrder>
  id?: InputMaybe<SortOrder>
  lastLogin?: InputMaybe<SortOrder>
  lastName?: InputMaybe<SortOrder>
  password?: InputMaybe<SortOrder>
  updated?: InputMaybe<SortOrder>
}

export enum UserScalarFieldEnum {
  Archived = 'archived',
  Created = 'created',
  Deleted = 'deleted',
  Email = 'email',
  FirstName = 'firstName',
  Id = 'id',
  LastLogin = 'lastLogin',
  LastName = 'lastName',
  Password = 'password',
  Updated = 'updated',
}

export type UserScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<InputMaybe<UserScalarWhereWithAggregatesInput>>>
  NOT?: InputMaybe<Array<InputMaybe<UserScalarWhereWithAggregatesInput>>>
  OR?: InputMaybe<Array<InputMaybe<UserScalarWhereWithAggregatesInput>>>
  created?: InputMaybe<DateTimeWithAggregatesFilter>
  email?: InputMaybe<StringWithAggregatesFilter>
  firstName?: InputMaybe<StringWithAggregatesFilter>
  id?: InputMaybe<StringWithAggregatesFilter>
  lastLogin?: InputMaybe<DateTimeNullableWithAggregatesFilter>
  lastName?: InputMaybe<StringWithAggregatesFilter>
  password?: InputMaybe<StringWithAggregatesFilter>
  updated?: InputMaybe<DateTimeWithAggregatesFilter>
}

export type UserUncheckedCreateInput = {
  created?: InputMaybe<Scalars['DateTime']>
  email: Scalars['String']
  firstName?: InputMaybe<Scalars['String']>
  id?: InputMaybe<Scalars['String']>
  lastLogin?: InputMaybe<Scalars['DateTime']>
  lastName?: InputMaybe<Scalars['String']>
  password: Scalars['String']
  updated?: InputMaybe<Scalars['DateTime']>
}

export type UserUncheckedUpdateInput = {
  created?: InputMaybe<DateTimeFieldUpdateOperationsInput>
  email?: InputMaybe<StringFieldUpdateOperationsInput>
  firstName?: InputMaybe<StringFieldUpdateOperationsInput>
  id?: InputMaybe<StringFieldUpdateOperationsInput>
  lastLogin?: InputMaybe<NullableDateTimeFieldUpdateOperationsInput>
  lastName?: InputMaybe<StringFieldUpdateOperationsInput>
  password?: InputMaybe<StringFieldUpdateOperationsInput>
  updated?: InputMaybe<DateTimeFieldUpdateOperationsInput>
}

export type UserUncheckedUpdateManyInput = {
  created?: InputMaybe<DateTimeFieldUpdateOperationsInput>
  email?: InputMaybe<StringFieldUpdateOperationsInput>
  firstName?: InputMaybe<StringFieldUpdateOperationsInput>
  id?: InputMaybe<StringFieldUpdateOperationsInput>
  lastLogin?: InputMaybe<NullableDateTimeFieldUpdateOperationsInput>
  lastName?: InputMaybe<StringFieldUpdateOperationsInput>
  password?: InputMaybe<StringFieldUpdateOperationsInput>
  updated?: InputMaybe<DateTimeFieldUpdateOperationsInput>
}

export type UserUpdateInput = {
  created?: InputMaybe<DateTimeFieldUpdateOperationsInput>
  email?: InputMaybe<StringFieldUpdateOperationsInput>
  firstName?: InputMaybe<StringFieldUpdateOperationsInput>
  id?: InputMaybe<StringFieldUpdateOperationsInput>
  lastLogin?: InputMaybe<NullableDateTimeFieldUpdateOperationsInput>
  lastName?: InputMaybe<StringFieldUpdateOperationsInput>
  password?: InputMaybe<StringFieldUpdateOperationsInput>
  updated?: InputMaybe<DateTimeFieldUpdateOperationsInput>
}

export type UserUpdateManyMutationInput = {
  created?: InputMaybe<DateTimeFieldUpdateOperationsInput>
  email?: InputMaybe<StringFieldUpdateOperationsInput>
  firstName?: InputMaybe<StringFieldUpdateOperationsInput>
  id?: InputMaybe<StringFieldUpdateOperationsInput>
  lastLogin?: InputMaybe<NullableDateTimeFieldUpdateOperationsInput>
  lastName?: InputMaybe<StringFieldUpdateOperationsInput>
  password?: InputMaybe<StringFieldUpdateOperationsInput>
  updated?: InputMaybe<DateTimeFieldUpdateOperationsInput>
}

export type UserWhereInput = {
  AND?: InputMaybe<Array<InputMaybe<UserWhereInput>>>
  NOT?: InputMaybe<Array<InputMaybe<UserWhereInput>>>
  OR?: InputMaybe<Array<InputMaybe<UserWhereInput>>>
  created?: InputMaybe<DateTimeFilter>
  email?: InputMaybe<StringFilter>
  firstName?: InputMaybe<StringFilter>
  id?: InputMaybe<StringFilter>
  lastLogin?: InputMaybe<DateTimeNullableFilter>
  lastName?: InputMaybe<StringFilter>
  password?: InputMaybe<StringFilter>
  updated?: InputMaybe<DateTimeFilter>
}

export type UserWhereUniqueInput = {
  email?: InputMaybe<Scalars['String']>
  id?: InputMaybe<Scalars['String']>
}

export type UserFragment = { __typename?: 'User'; id: string; email: string; firstName: string; lastName: string }

export type UserFindManyQueryVariables = Exact<{
  where?: InputMaybe<UserWhereInput>
}>

export type UserFindManyQuery = {
  __typename?: 'Query'
  userFindManyQuery: Array<{ __typename?: 'User'; id: string; email: string; firstName: string; lastName: string }>
}

export type UserCreateOneMutationVariables = Exact<{
  data: UserCreateInput
}>

export type UserCreateOneMutation = {
  __typename?: 'Mutation'
  userCreateOneMutation: { __typename?: 'User'; id: string; email: string; firstName: string; lastName: string }
}

export const UserFragmentDoc = gql`
  fragment User on User {
    id
    email
    firstName
    lastName
  }
`
export const UserFindManyDocument = gql`
  query UserFindMany($where: UserWhereInput) {
    userFindManyQuery(where: $where) {
      ...User
    }
  }
  ${UserFragmentDoc}
`

/**
 * __useUserFindManyQuery__
 *
 * To run a query within a React component, call `useUserFindManyQuery` and pass it any options that fit your needs.
 * When your component renders, `useUserFindManyQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUserFindManyQuery({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useUserFindManyQuery(
  baseOptions?: Apollo.QueryHookOptions<UserFindManyQuery, UserFindManyQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions }
  return Apollo.useQuery<UserFindManyQuery, UserFindManyQueryVariables>(UserFindManyDocument, options)
}
export function useUserFindManyLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<UserFindManyQuery, UserFindManyQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions }
  return Apollo.useLazyQuery<UserFindManyQuery, UserFindManyQueryVariables>(UserFindManyDocument, options)
}
export type UserFindManyQueryHookResult = ReturnType<typeof useUserFindManyQuery>
export type UserFindManyLazyQueryHookResult = ReturnType<typeof useUserFindManyLazyQuery>
export type UserFindManyQueryResult = Apollo.QueryResult<UserFindManyQuery, UserFindManyQueryVariables>
export const UserCreateOneDocument = gql`
  mutation UserCreateOne($data: UserCreateInput!) {
    userCreateOneMutation(data: $data) {
      ...User
    }
  }
  ${UserFragmentDoc}
`
export type UserCreateOneMutationFn = Apollo.MutationFunction<UserCreateOneMutation, UserCreateOneMutationVariables>

/**
 * __useUserCreateOneMutation__
 *
 * To run a mutation, you first call `useUserCreateOneMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUserCreateOneMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [userCreateOneMutation, { data, loading, error }] = useUserCreateOneMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useUserCreateOneMutation(
  baseOptions?: Apollo.MutationHookOptions<UserCreateOneMutation, UserCreateOneMutationVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions }
  return Apollo.useMutation<UserCreateOneMutation, UserCreateOneMutationVariables>(UserCreateOneDocument, options)
}
export type UserCreateOneMutationHookResult = ReturnType<typeof useUserCreateOneMutation>
export type UserCreateOneMutationResult = Apollo.MutationResult<UserCreateOneMutation>
export type UserCreateOneMutationOptions = Apollo.BaseMutationOptions<
  UserCreateOneMutation,
  UserCreateOneMutationVariables
>
