/**
 * Docs:
 *   https://www.apollographql.com/docs/react/get-started/#3-connect-your-client-to-react
 */

import { ApolloClient, InMemoryCache } from '@apollo/client'

// TODO: use possible .env values here.
const uri = 'http://localhost:3001/graphql'

export const apolloClient = new ApolloClient({
  uri,
  cache: new InMemoryCache(),
})
