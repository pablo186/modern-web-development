import { ReactNode } from 'react'

interface IPublic {
  children: ReactNode
}

export const Public = (props: IPublic) => {
  const { children } = props

  return <>{children}</>
}
