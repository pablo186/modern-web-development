import { ReactNode } from 'react'

interface IPrivate {
  children: ReactNode
}

export const Private = (props: IPrivate) => {
  const { children } = props

  return <>{children}</>
}
