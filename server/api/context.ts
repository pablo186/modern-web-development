import { PrismaClient } from '@prisma/client'
import { Request, Response } from 'express'
import { prisma } from './prisma'

export interface Context {
  request: Request
  response: Response
  prisma: PrismaClient
  select: any
}

export const context = ({ req, res }: { req: Request; res: Response }): Context => {
  return {
    request: req,
    response: res,
    prisma,
    select: {},
  }
}
