import { ApolloServer } from 'apollo-server-express'
import express from 'express'
import { join } from 'path'
// API - GraphQL
import { context } from './context'
import { schema } from './schema'
// API - Rest
import { restApiRouter } from './rest'
/**
 * Express Server
 */
export const app = express() as any
export const server = new ApolloServer({ schema, context })
/**
 * Async Server
 */
;(async function () {
  await server.start()
  server.applyMiddleware({ app })

  /**
   * Happy Hacking!!!
   */

  if (process.env.NODE_ENV === 'production') {
    app.use(express.static(join('..', 'client', 'build')))
  }

  // GraphQL
  app.use(express.json())
  // Rest
  app.use('/rest', restApiRouter)
})()
