import { join } from 'path'
import { makeSchema } from 'nexus'
import { paljs } from '@paljs/nexus'

import * as types from './graphql'

export const schema: any = makeSchema({
  types,
  outputs: {
    typegen: join(__dirname, 'generated/nexus-typegen.ts'),
    schema: join(__dirname, 'generated/schema.graphql'),
  },
  contextType: {
    module: join(__dirname, 'context.ts'),
    export: 'Context',
  },
  prettierConfig: join(__dirname, '../../package.json'),
  plugins: [paljs({ excludeFields: ['creatorId', 'archived', 'deleted'] })],
})
