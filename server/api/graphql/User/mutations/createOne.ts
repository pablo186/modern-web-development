import { mutationField, nonNull } from 'nexus'

export const UserCreateOneMutation = mutationField('userCreateOneMutation', {
  type: nonNull('User'),
  args: {
    data: nonNull('UserCreateInput'),
  },
  async resolve(_root, { data }, { prisma, select }) {
    return await prisma.user.create({ data: { ...data }, ...select })
  },
})
