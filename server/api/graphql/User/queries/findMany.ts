import { list, nonNull, queryField } from 'nexus'

export const UserFindManyQuery = queryField('userFindManyQuery', {
  type: nonNull(list(nonNull('User'))),
  args: {
    where: 'UserWhereInput',
  },
  async resolve(_root, args, { prisma, select }) {
    const { where } = args
    return await prisma.user.findMany({ where, ...args, ...select })
  },
})
