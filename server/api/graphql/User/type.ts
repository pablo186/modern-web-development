import { objectType } from 'nexus'

export const User = objectType({
  nonNullDefaults: {
    output: true,
    input: false,
  },
  name: 'User',
  definition: t => {
    t.string('id')
    t.string('email')
    t.string('firstName')
    t.string('lastName')
    t.field('created', { type: 'DateTime' })
    t.field('updated', { type: 'DateTime' })
  },
})
