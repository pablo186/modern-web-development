import cuid from 'cuid'
import { createTestContext } from './__helpers'

const ctx = createTestContext()

it('test User/mutations/createOne', async () => {
  const userEmail = `test${cuid()}@test.com`
  // create user.
  const userResult = await ctx.client.request(`
    mutation {
      userCreateOneMutation(data: { email: "${userEmail}", firstName: "test", lastName: "test", password: "P4zzW0rD" }) {
        id
        email
        firstName
        lastName
      }
    }
  `)

  // expect `userResult.userCreateOneMutation.id` and `userEmail` to match.
  expect(userResult).toMatchObject({
    userCreateOneMutation: {
      email: userEmail,
      firstName: 'test',
      id: userResult.userCreateOneMutation.id,
      lastName: 'test',
    },
  })
})
