/**
 * https://www.prisma.io/docs/guides/database/seed-database
 *
 */
import { PrismaClient } from '@prisma/client'

import { USERS } from './seeds/users'

const prisma = new PrismaClient()

const runSeeder = async () => {
  await prisma.user.createMany({ data: USERS })
}

runSeeder()
  .catch(error => {
    console.error(error)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
