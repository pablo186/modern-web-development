/**
 * https://www.prisma.io/docs/guides/database/seed-database
 *
 */
import { Prisma } from '@prisma/client'

export const USERS = [
  {
    id: '1111111111',
    firstName: 'Frédéric',
    lastName: 'Bastiat',
    email: 'bastiat@example.com',
    password: 'pazzW0rd',
  },
  {
    id: '2222222222',
    firstName: 'Carl',
    lastName: 'Menger',
    email: 'menger@example.com',
    password: 'pazzW0rd',
  },
  {
    id: '3333333333',
    firstName: 'Ludwig',
    lastName: 'von Mises',
    email: 'mises@example.com',
    password: 'pazzW0rd',
  },
  {
    id: '4444444444',
    firstName: 'Murray',
    lastName: 'Rothbard',
    email: 'rothbard@example.com',
    password: 'pazzW0rd',
  },
  {
    id: '5555555555',
    firstName: 'Jesús',
    lastName: 'Huerta de Soto',
    email: 'jesus@example.com',
    password: 'pazzW0rd',
  },
  {
    id: '6666666666',
    firstName: 'Jeffrey',
    lastName: 'Tucker',
    email: 'tucker@example.com',
    password: 'pazzW0rd',
  },
  {
    id: '7777777777',
    firstName: 'Jeff',
    lastName: 'Deist',
    email: 'deist@example.com',
    password: 'pazzW0rd',
  },
] as Prisma.UserCreateManyInput[]
